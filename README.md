There's two files, one for each frequency.

Each file contains one array, of shape 1000000x2x3.

* 1000000 are the measured data points in one run
  * the sampling frequency is 5000 Hz
* 2 are the channels
  * 1 is the reference signal in Volt
  * 2 is the velocity in mm/s
* 3 are the voltages at which the test has been run
  * 1 is 1.2 Volt
  * 2 is 1.3 Volt
  * 3 is 1.4 Volt

Say you wanted to look at the time record of the vibrometer data at 240 Hz, 1.4 Volt...(HINT: you DO want to look at such data, Peter) you'd have to...

`load time_records_240_hz.mat
plot (t_240_hz(:,2,3))`

Let's have a meeting!
